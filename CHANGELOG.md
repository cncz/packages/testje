# Change Log

All notable changes to this publication will be documented in this file.

## 0.1.1 - 2021-04-30
Several updates for a 'clean slate'

## 0.1.0 - 2021-04-29

First stable release.
