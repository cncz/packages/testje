#!/bin/bash

# We need to install dependencies only for Docker
[[ ! -e /.dockerenv ]] && exit 0

set -xe

# Install git (the php image doesn't have it) which is required by composer
apt-get update -yqq
apt-get install git -yqq
apt-get install curl -yqq
apt-get install zip unzip -yqq

pecl install xdebug && docker-php-ext-enable xdebug

# Install phpunit, the tool that we will use for testing
curl --location --output /usr/local/bin/phpunit -s "https://phar.phpunit.de/phpunit-9.6.phar"
chmod +x /usr/local/bin/phpunit

curl --silent --show-error "https://getcomposer.org/installer" | php -- --install-dir=/usr/local/bin --filename=composer
composer install --no-ansi --no-dev --no-interaction --no-plugins --no-progress --no-scripts --optimize-autoloader

